const TranslationExport = require('../models').TranslationExport;

function findById(videoId) {
    return TranslationExport.findById(videoId)
}

function update(conditions, keyValMap) {
    return TranslationExport.updateMany(conditions, { $set: keyValMap })
}

function updateById(id, keyValMap) {
    return TranslationExport.findByIdAndUpdate(id, { $set: keyValMap }, { new: true })
}

function find(conditions) {
    return TranslationExport.find(conditions)
}

function create(values) {
    return TranslationExport.create(values);
}

module.exports = {
    find,
    update,
    create,
    findById,
    updateById,
}
